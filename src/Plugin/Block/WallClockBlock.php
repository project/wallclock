<?php

namespace Drupal\wallclock\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides a block to contain the wallclock.
 *
 * @Block(
 *   id = "wallclock",
 *   admin_label = @Translation("Wall Clock Block"),
 * )
 */
class WallClockBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */

  protected $configfactory;

  /**
   * {@inheritdoc}
   *
   * @param array $configuration
   *   Configuration variables in array.
   * @param string $plugin_id
   *   Id of the plugin.
   * @param mixed $plugin_definition
   *   Definition of the plugin.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config variables declairation.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configfactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#cache' => ['max-age' => 0],
      '#theme' => 'wallclock',
    ];
  }

}
