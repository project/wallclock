# Wallclock

This module provides a block that displays current
system time as a analog wallclock.

For a full description of the module, visit the [project
page](https://www.drupal.org/project/wallclock).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/wallclock).


## Contents of this file

 * Requirements
 * Installation
 * Configuration
 * Theming


## Requirements

 * This module uses the JavaScript library `snap.svg` to display the
   analog clock. This must be installed manually.


## Installation

1. Install as you would normally install a contributed Drupal
   module. Visit [Installing
   modules](https://www.drupal.org/node/1897420) for further
   information.

2. Install the latest compatible version of the 'snap.svg' Javascript
   library.  Visit [snapsvg.io](http://snapsvg.io/), and click on
   "Download". Then unpack the zipfile and install
   "dist/snap-svg-min.js" in a directory named "snap.svg" in the
   direcory "libraries" below the webroot. The path to the library
   from the the webroot should be
   "libraries/snap.svg/snap-svg-min.js".

   Version 0.5.1 has been tested with this module and found to
   work. You may download "snap-svg-min.js" of this version from from
   [CDN](http://cdnjs.cloudflare.com/ajax/libs/snap.svg/0.5.1/snap.svg-min.js)

3. Enable the **Wallclock** module by locating it on the list under
   the Extend tab in the administrative GUI.


## Configuration

To display a wallclock, navigate to **Manage » Structure » Block
layout** and click on "Place block" for the region where you want the
clock to display.  It is named "Wall Clock Block".


## Theming

See `wallclock.libraries.yml` for the CSS and JS libraries used. For
unknown reasons aggregation (`preprocess: false`) must be turned
offfor the CSS.

The CSS uses flex for layout and probably works best with a theme
supporting this, such as
[**Bootstrap5**](https://www.drupal.org/project/bootstrap5).

It assumes that the Segoe UI font is available. If it is not, it will
fall back in the default sans-serif font.

