(function ($) {
  'use strict';
  Drupal.behaviors.awesome = {
    attach: function (context, settings) {
      function startTime() {
        var today = new Date();
        var hr = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();
        min = checkTime(min);
        sec = checkTime(sec);
        jQuery('#digital #clock').html(hr + ' : ' + min);
        setTimeout(function () { startTime(); }, 500);
      }
      function checkTime(i) {
        if (i < 10) {
          i = '0' + i;
        }
        return i;
      }
      startTime();
    }
  };
}(jQuery));
